﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyLoading
{
    public class LazyLoadingDatabaseContext : DbContext 
    {
        public LazyLoadingDatabaseContext() 
            : base ("LazyLoadingDatabaseContext")
        { }

        public DbSet<Player> Players { get;set;}
        public DbSet<Team> Teams { get; set; }
    }
}
