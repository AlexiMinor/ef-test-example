﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyLoading
{
    class Program
    {
        static void Main(string[] args)
        {
            using (LazyLoadingDatabaseContext db = new LazyLoadingDatabaseContext())
            {
                Team t1 = new Team { Id = Guid.NewGuid(), Name = "team 1" };
                Team t2 = new Team { Id = Guid.NewGuid(), Name = "team 2" };

                db.Teams.Add(t1);
                db.Teams.Add(t2);
                db.SaveChanges();

                Player pl1 = new Player { Id = Guid.NewGuid(), Name = "player 1", Age = 20, Team = t1 };
                Player pl2 = new Player { Id = Guid.NewGuid(), Name = "player 2", Age = 22, Team = t2 };
                Player pl3 = new Player { Id = Guid.NewGuid(), Name = "player 3", Age = 23, Team = t1 };
                Player pl4 = new Player { Id = Guid.NewGuid(), Name = "player 4", Age = 25, Team = t2 };

                db.Players.AddRange(new List<Player> { pl1, pl2, pl3, pl4 });
                db.SaveChanges();
            }
            using (LazyLoadingDatabaseContext db = new LazyLoadingDatabaseContext())
            {
                var teams = db.Teams.ToList();
                foreach (var a  in db.Players.Include(x=>x.Team))
                {

                    Console.WriteLine($"{a.Name} - {a.Team.Name}");
                }
            }
        }
    }
}
