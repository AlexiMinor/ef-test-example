﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class BookController : Controller
    {
        BookContext _db = new BookContext();
        // GET: Book
        public ActionResult Index()
        {
            IEnumerable<Book> books = _db.Books;

            ViewBag.Books = books;

            return View();
        }
    }
}