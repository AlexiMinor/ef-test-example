﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApplication1.Models
{
    public class DBInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            context.Books.Add(new Book { Name = "CLR via C#", Author = "J.Richter", Price = 50 });
            context.Books.Add(new Book { Name = "ASP.Net for professionals", Author = "A.Friman", Price = 150 });

            base.Seed(context);
        }
    }
}