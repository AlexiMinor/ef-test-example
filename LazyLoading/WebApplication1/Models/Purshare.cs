﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Purshare
    {
        public int Id { get; set; }

        public string PersonName { get; set; }

        public string Address { get; set; }

        public int BookId { get; set; }
        public DateTime Date { get; set; }

    }
}